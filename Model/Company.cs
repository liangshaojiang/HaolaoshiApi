﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //校园招聘企业库
    [Serializable]
    [Table("Company")]
    public class Company : SchoolUserTeacherID
    {     
        [Display(Name = "企业名称")]
        [Required(ErrorMessage = "名称必填")]
        public string Name { get; set; }//企业名称
        [Display(Name = "封面")]
        public string Pic { get; set; }    
        [Display(Name = "企业规模")]
        public int? Scale { get; set; }//企业规模
        [Display(Name = "简介")]
        [DataType(DataType.MultilineText)]
        public string Memo { get; set; }//备注
        [Display(Name = "附件")]
        public string Attachment { get; set; }//附件
    }
}