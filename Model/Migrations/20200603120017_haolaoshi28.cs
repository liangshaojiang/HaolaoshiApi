﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi28 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TimeTable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ClassSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Classroom",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_UserId",
                table: "TimeTable",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSchedule_UserId",
                table: "ClassSchedule",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_UserId",
                table: "Classroom",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_User_UserId",
                table: "Classroom",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_User_UserId",
                table: "ClassSchedule",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_User_UserId",
                table: "TimeTable",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_User_UserId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_User_UserId",
                table: "ClassSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_User_UserId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_UserId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_ClassSchedule_UserId",
                table: "ClassSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_UserId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Classroom");
        }
    }
}
