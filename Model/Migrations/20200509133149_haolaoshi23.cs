﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "UsualScoreLog",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "UsualScoreItem",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "UsualScore",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "User",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Teacher",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Student",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Score",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "School",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "RoleAuthority",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Role",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Res",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Post",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "OnlineResume",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Major",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Interview",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Images",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Group",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Grade",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "CourseRes",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Course",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Company",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "College",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "ClockLog",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Clock",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Classs",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Chapter",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Category",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "AuthorityRes",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Authority",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Article",
                newName: "Added_time");

            migrationBuilder.RenameColumn(
                name: "added_time",
                table: "Area",
                newName: "Added_time");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UsualScoreLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UsualScoreItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UsualScore",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Score",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "School",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "RoleAuthority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Role",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Res",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "OnlineResume",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Major",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Interview",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Grade",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "College",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Clock",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Classs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Category",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "AuthorityRes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Authority",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_UserId",
                table: "UsualScoreLog",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreItem_UserId",
                table: "UsualScoreItem",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScore_UserId",
                table: "UsualScore",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Score_UserId",
                table: "Score",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_School_UserId",
                table: "School",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_UserId",
                table: "RoleAuthority",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_UserId",
                table: "Role",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_UserId",
                table: "Res",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_UserId",
                table: "Post",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlineResume_UserId",
                table: "OnlineResume",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_UserId",
                table: "Major",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Interview_UserId",
                table: "Interview",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_UserId",
                table: "Images",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_UserId",
                table: "Group",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_UserId",
                table: "Grade",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_UserId",
                table: "Company",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_College_UserId",
                table: "College",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_UserId",
                table: "Clock",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Classs_UserId",
                table: "Classs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_UserId",
                table: "Category",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_UserId",
                table: "AuthorityRes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_UserId",
                table: "Authority",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_User_UserId",
                table: "Authority",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_User_UserId",
                table: "AuthorityRes",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_User_UserId",
                table: "Category",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classs_User_UserId",
                table: "Classs",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_User_UserId",
                table: "Clock",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_College_User_UserId",
                table: "College",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_User_UserId",
                table: "Company",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_User_UserId",
                table: "Grade",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_User_UserId",
                table: "Group",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_User_UserId",
                table: "Images",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_User_UserId",
                table: "Interview",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_User_UserId",
                table: "Major",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineResume_User_UserId",
                table: "OnlineResume",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_User_UserId",
                table: "Post",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_User_UserId",
                table: "Res",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_User_UserId",
                table: "Role",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_User_UserId",
                table: "RoleAuthority",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_School_User_UserId",
                table: "School",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_User_UserId",
                table: "Score",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScore_User_UserId",
                table: "UsualScore",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreItem_User_UserId",
                table: "UsualScoreItem",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreLog_User_UserId",
                table: "UsualScoreLog",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authority_User_UserId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_User_UserId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_User_UserId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Classs_User_UserId",
                table: "Classs");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_User_UserId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_College_User_UserId",
                table: "College");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_User_UserId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_User_UserId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_User_UserId",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Images_User_UserId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Interview_User_UserId",
                table: "Interview");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_User_UserId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_OnlineResume_User_UserId",
                table: "OnlineResume");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_User_UserId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_User_UserId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_User_UserId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_User_UserId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_School_User_UserId",
                table: "School");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_User_UserId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScore_User_UserId",
                table: "UsualScore");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreItem_User_UserId",
                table: "UsualScoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreLog_User_UserId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreLog_UserId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreItem_UserId",
                table: "UsualScoreItem");

            migrationBuilder.DropIndex(
                name: "IX_UsualScore_UserId",
                table: "UsualScore");

            migrationBuilder.DropIndex(
                name: "IX_Score_UserId",
                table: "Score");

            migrationBuilder.DropIndex(
                name: "IX_School_UserId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_UserId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_Role_UserId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Res_UserId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Post_UserId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_OnlineResume_UserId",
                table: "OnlineResume");

            migrationBuilder.DropIndex(
                name: "IX_Major_UserId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Interview_UserId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Images_UserId",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Group_UserId",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Grade_UserId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_Company_UserId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_College_UserId",
                table: "College");

            migrationBuilder.DropIndex(
                name: "IX_Clock_UserId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_Classs_UserId",
                table: "Classs");

            migrationBuilder.DropIndex(
                name: "IX_Category_UserId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_UserId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_Authority_UserId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "School");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Authority");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "UsualScoreLog",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "UsualScoreItem",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "UsualScore",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "User",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Teacher",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Student",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Score",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "School",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "RoleAuthority",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Role",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Res",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Post",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "OnlineResume",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Major",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Interview",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Images",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Group",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Grade",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "CourseRes",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Course",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Company",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "College",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "ClockLog",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Clock",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Classs",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Chapter",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Category",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "AuthorityRes",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Authority",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Article",
                newName: "added_time");

            migrationBuilder.RenameColumn(
                name: "Added_time",
                table: "Area",
                newName: "added_time");
        }
    }
}
