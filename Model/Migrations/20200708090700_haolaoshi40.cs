﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi40 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Chapter",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Chapter_SchoolId",
                table: "Chapter",
                column: "SchoolId");

            migrationBuilder.AddForeignKey(
                name: "FK_Chapter_School_SchoolId",
                table: "Chapter",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chapter_School_SchoolId",
                table: "Chapter");

            migrationBuilder.DropIndex(
                name: "IX_Chapter_SchoolId",
                table: "Chapter");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Chapter");
        }
    }
}
