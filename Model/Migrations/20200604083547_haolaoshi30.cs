﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi30 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Sn",
                table: "Course",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CourseSn",
                table: "ClassSchedule",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Sn",
                table: "Classs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sn",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "CourseSn",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "Sn",
                table: "Classs");
        }
    }
}
