﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi36 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "UsualScoreLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScoreLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "UsualScoreItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScoreItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScore",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "TimeTable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "TimeTable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Slider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Slider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Score",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "School",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "School",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "RoleAuthority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "RoleAuthority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Role",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Role",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Res",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Res",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "OnlineResume",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Major",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Major",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Interview",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Interview",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Grade",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Grade",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "College",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "College",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Clock",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Clock",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "ClassSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Classs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Classroom",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Classroom",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Category",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Category",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "AuthorityRes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "AuthorityRes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Authority",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Authority",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_StudentId",
                table: "UsualScoreLog",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_TeacherId",
                table: "UsualScoreLog",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreItem_StudentId",
                table: "UsualScoreItem",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreItem_TeacherId",
                table: "UsualScoreItem",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScore_TeacherId",
                table: "UsualScore",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_StudentId",
                table: "TimeTable",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_TeacherId",
                table: "TimeTable",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_StudentId",
                table: "Slider",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_TeacherId",
                table: "Slider",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Score_TeacherId",
                table: "Score",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_School_StudentId",
                table: "School",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_School_TeacherId",
                table: "School",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_StudentId",
                table: "RoleAuthority",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_TeacherId",
                table: "RoleAuthority",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_StudentId",
                table: "Role",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_TeacherId",
                table: "Role",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_StudentId",
                table: "Res",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_TeacherId",
                table: "Res",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_StudentId",
                table: "Post",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_TeacherId",
                table: "Post",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlineResume_TeacherId",
                table: "OnlineResume",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_StudentId",
                table: "Major",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_TeacherId",
                table: "Major",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Interview_StudentId",
                table: "Interview",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_StudentId",
                table: "Images",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_TeacherId",
                table: "Images",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_StudentId",
                table: "Group",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_TeacherId",
                table: "Group",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_StudentId",
                table: "Grade",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_TeacherId",
                table: "Grade",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_StudentId",
                table: "Company",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_TeacherId",
                table: "Company",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_College_StudentId",
                table: "College",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_College_TeacherId",
                table: "College",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_StudentId",
                table: "Clock",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_TeacherId",
                table: "Clock",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSchedule_StudentId",
                table: "ClassSchedule",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classs_StudentId",
                table: "Classs",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_StudentId",
                table: "Classroom",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_TeacherId",
                table: "Classroom",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_StudentId",
                table: "Category",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_TeacherId",
                table: "Category",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_StudentId",
                table: "AuthorityRes",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_TeacherId",
                table: "AuthorityRes",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_StudentId",
                table: "Authority",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_TeacherId",
                table: "Authority",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_Student_StudentId",
                table: "Authority",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_Teacher_TeacherId",
                table: "Authority",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_Student_StudentId",
                table: "AuthorityRes",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_Teacher_TeacherId",
                table: "AuthorityRes",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Student_StudentId",
                table: "Category",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Teacher_TeacherId",
                table: "Category",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_Student_StudentId",
                table: "Classroom",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_Teacher_TeacherId",
                table: "Classroom",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classs_Student_StudentId",
                table: "Classs",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_Student_StudentId",
                table: "ClassSchedule",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_Student_StudentId",
                table: "Clock",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_Teacher_TeacherId",
                table: "Clock",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_College_Student_StudentId",
                table: "College",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_College_Teacher_TeacherId",
                table: "College",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Student_StudentId",
                table: "Company",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_Student_StudentId",
                table: "Grade",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_Teacher_TeacherId",
                table: "Grade",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Student_StudentId",
                table: "Group",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_Teacher_TeacherId",
                table: "Group",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Student_StudentId",
                table: "Images",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_Teacher_TeacherId",
                table: "Images",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Student_StudentId",
                table: "Interview",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_Student_StudentId",
                table: "Major",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_Teacher_TeacherId",
                table: "Major",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineResume_Teacher_TeacherId",
                table: "OnlineResume",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Student_StudentId",
                table: "Post",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_Student_StudentId",
                table: "Res",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_Teacher_TeacherId",
                table: "Res",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Student_StudentId",
                table: "Role",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Teacher_TeacherId",
                table: "Role",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_Student_StudentId",
                table: "RoleAuthority",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_Teacher_TeacherId",
                table: "RoleAuthority",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_School_Student_StudentId",
                table: "School",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_School_Teacher_TeacherId",
                table: "School",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Teacher_TeacherId",
                table: "Score",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Slider_Student_StudentId",
                table: "Slider",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Slider_Teacher_TeacherId",
                table: "Slider",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_Student_StudentId",
                table: "TimeTable",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_Teacher_TeacherId",
                table: "TimeTable",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScore_Teacher_TeacherId",
                table: "UsualScore",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreItem_Student_StudentId",
                table: "UsualScoreItem",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreItem_Teacher_TeacherId",
                table: "UsualScoreItem",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreLog_Student_StudentId",
                table: "UsualScoreLog",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreLog_Teacher_TeacherId",
                table: "UsualScoreLog",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authority_Student_StudentId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_Authority_Teacher_TeacherId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_Student_StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_Teacher_TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_Student_StudentId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_Teacher_TeacherId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_Student_StudentId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_Teacher_TeacherId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_Classs_Student_StudentId",
                table: "Classs");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_Student_StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_Student_StudentId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_Teacher_TeacherId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_College_Student_StudentId",
                table: "College");

            migrationBuilder.DropForeignKey(
                name: "FK_College_Teacher_TeacherId",
                table: "College");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Student_StudentId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_Student_StudentId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_Teacher_TeacherId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_Student_StudentId",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_Teacher_TeacherId",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Images_Student_StudentId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Images_Teacher_TeacherId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Student_StudentId",
                table: "Interview");

            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_Student_StudentId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_Teacher_TeacherId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_OnlineResume_Teacher_TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Student_StudentId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_Student_StudentId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_Teacher_TeacherId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_Student_StudentId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_Teacher_TeacherId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_Student_StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_Teacher_TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_School_Student_StudentId",
                table: "School");

            migrationBuilder.DropForeignKey(
                name: "FK_School_Teacher_TeacherId",
                table: "School");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_Teacher_TeacherId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_Slider_Student_StudentId",
                table: "Slider");

            migrationBuilder.DropForeignKey(
                name: "FK_Slider_Teacher_TeacherId",
                table: "Slider");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_Student_StudentId",
                table: "TimeTable");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_Teacher_TeacherId",
                table: "TimeTable");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScore_Teacher_TeacherId",
                table: "UsualScore");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreItem_Student_StudentId",
                table: "UsualScoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreItem_Teacher_TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreLog_Student_StudentId",
                table: "UsualScoreLog");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreLog_Teacher_TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreLog_StudentId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreLog_TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreItem_StudentId",
                table: "UsualScoreItem");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreItem_TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropIndex(
                name: "IX_UsualScore_TeacherId",
                table: "UsualScore");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_StudentId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_TeacherId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_Slider_StudentId",
                table: "Slider");

            migrationBuilder.DropIndex(
                name: "IX_Slider_TeacherId",
                table: "Slider");

            migrationBuilder.DropIndex(
                name: "IX_Score_TeacherId",
                table: "Score");

            migrationBuilder.DropIndex(
                name: "IX_School_StudentId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_School_TeacherId",
                table: "School");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_Role_StudentId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Role_TeacherId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Res_StudentId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Res_TeacherId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Post_StudentId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Post_TeacherId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_OnlineResume_TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropIndex(
                name: "IX_Major_StudentId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Major_TeacherId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Interview_StudentId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Images_StudentId",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Images_TeacherId",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Group_StudentId",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Group_TeacherId",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Grade_StudentId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_Grade_TeacherId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_Company_StudentId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_TeacherId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_College_StudentId",
                table: "College");

            migrationBuilder.DropIndex(
                name: "IX_College_TeacherId",
                table: "College");

            migrationBuilder.DropIndex(
                name: "IX_Clock_StudentId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_Clock_TeacherId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_ClassSchedule_StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Classs_StudentId",
                table: "Classs");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_StudentId",
                table: "Classroom");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_TeacherId",
                table: "Classroom");

            migrationBuilder.DropIndex(
                name: "IX_Category_StudentId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Category_TeacherId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_Authority_StudentId",
                table: "Authority");

            migrationBuilder.DropIndex(
                name: "IX_Authority_TeacherId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "School");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "School");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Authority");
        }
    }
}
