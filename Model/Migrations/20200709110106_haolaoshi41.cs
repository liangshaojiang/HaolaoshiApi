﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi41 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CourseClass",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Added_time = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    TeacherId = table.Column<int>(nullable: true),
                    SchoolId = table.Column<int>(nullable: true),
                    ClasssId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseClass", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CourseClass_Classs_ClasssId",
                        column: x => x.ClasssId,
                        principalTable: "Classs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseClass_Course_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Course",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseClass_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseClass_Teacher_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teacher",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseClass_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CourseClass_ClasssId",
                table: "CourseClass",
                column: "ClasssId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseClass_CourseId",
                table: "CourseClass",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseClass_SchoolId",
                table: "CourseClass",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseClass_TeacherId",
                table: "CourseClass",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseClass_UserId",
                table: "CourseClass",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CourseClass");
        }
    }
}
