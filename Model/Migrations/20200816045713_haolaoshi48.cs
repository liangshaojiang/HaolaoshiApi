﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi48 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Interview",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Interview");
        }
    }
}
