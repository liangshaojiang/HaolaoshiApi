using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class ClassScheduleBll : BaseBll<ClassSchedule>, IClassScheduleBll
    {
        public ClassScheduleBll(IClassScheduleDAL dal):base(dal)
        {
        }
    }
}
