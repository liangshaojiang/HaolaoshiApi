using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class GroupBll : BaseBll<Group>, IGroupBll
    {
        public GroupBll(IGroupDAL dal):base(dal)
        {
        }
    }
}
