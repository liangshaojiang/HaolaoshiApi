using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class SliderDAL : BaseDAL<Slider>, ISliderDAL
    {
        public SliderDAL(MyDbContext db) : base(db)
        {
        }
    }
}
