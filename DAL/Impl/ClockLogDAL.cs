using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class ClockLogDAL : BaseDAL<ClockLog>, IClockLogDAL
    {
        public ClockLogDAL(MyDbContext db) : base(db)
        {
        }
    }
}
