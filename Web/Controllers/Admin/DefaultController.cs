﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bll;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using Web.Filter;
using Web.Redis;
using Web.Security;

namespace Web.Controllers.Admin
{
    [Route("api/admin/")]
    [ApiController]
    [Authorize("admin")]
    [QueryFilter]
    public class DefaultController : MyBaseController<Object>
    {
        public IStudentBll studentBll { get; set; }
        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        [HttpGet("statistics")]
        public Result Statistics()
        {
            var db = studentBll.DbContext();
            return Result.Success("succeed").SetData(new { studentCount = db.Students.Count(), teacherCount = db.Teachers.Count(),courseCount=db.Courses.Count(),classCount=db.Classss.Count() });
        }
    }
}