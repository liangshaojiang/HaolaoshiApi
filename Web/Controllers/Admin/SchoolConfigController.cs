/* author:QinYongcheng */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using Web.Controllers.Util;

namespace Web.Admin.Controllers
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [Authorize("admin")]
    [QueryFilter]
    public class SchoolConfigController : MyBaseController<SchoolConfig>
    {
        ISchoolConfigBll bll;
        public SchoolConfigController(ISchoolConfigBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 获取当前学校的配置参数，如是以系统管理员登录，或者默认配置
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public Result Get()
        {
            return Result.Success("succeed").SetData(bll.SelectOneBySchoolOrInit(MyUser.SchoolId.GetValueOrDefault()));
        }
        [HttpPost]
        public Result Update(SchoolConfig o)
        {
            bll.Update(o);
            EntityCacheHelper.GetInstance().Put(o);//放入缓存
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
    }
}
