﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Redis;
using Web.Filter;
using Microsoft.EntityFrameworkCore;

namespace Web.Teach.Controllers
{

    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [QueryFilter]
    public class ClassScheduleController : MyBaseController<ClassSchedule>
    {
        IClassScheduleBll bll;
        public ClassScheduleController(IClassScheduleBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 获取今天的课程安排表
        /// </summary>
        /// <returns></returns>     
        [HttpPost]
        public Result Today()
        {
            MyRedisHelper rd = MyRedisHelper.Instance();
            rd.SetSysCustomKey("clock_stu");//已签到的学生集合     
            var rightanswer = rd.StringGet<string>(MyUser.Id+ "_teacher_answer");//签到答案           
            return Result.Success("succeed").SetData(new { clockanswer=rightanswer,schedule= bll.SelectAll(o => o.Date == DateTime.Now.Date && o.TeacherId == MyUser.Id) });
        }
        /// <summary>
        /// 获取课时数
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Result ClassHours(int pageSize=6)
        {
            var db = bll.DbContext();
            var query = from p in db.ClassSchedules where p.TeacherId== MyUser.Id
                        group p by new {p.Date.Year,p.Date.Month } into g orderby g.Key.Year descending,g.Key.Month descending
                        select new
                        {
                            Year = g.Key.Year,
                            Month = g.Key.Month,
                            Count = g.Count()
                        }; 

            var objs = query.Take(pageSize).ToList();
            return Result.Success("succeed").SetData(objs);
        }

    }
}
